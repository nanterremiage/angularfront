import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utilisateur } from '../entite/utilisateur';
import { AuthentificationService } from '../services/authentification.service';
import { OrdinateursService } from '../services/ordinateurs.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {

  utilisateur:Utilisateur;
  error:any;

  constructor(private ordiService: OrdinateursService,private router: Router, private authService: AuthentificationService) { 
    this.utilisateur = <Utilisateur>{ };
  }

  ngOnInit(): void {
    this.error=null;
  }

  connexion(connexionForm:any){
    this.utilisateur.email=connexionForm.value.email;
    this.utilisateur.password=connexionForm.value.passwd;
    //console.log(connexionForm);
    this.ordiService.connexion(this.utilisateur).subscribe(result => {
      //console.log(this.utilisateur.email);
      if(this.utilisateur.email == result.email){
        this.authService.connect(result);
        this.router.navigate(['']);
      }
      this.error="error";
    });
  }

}