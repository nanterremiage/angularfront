import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NouveauArticleFormComponent } from './nouveau-article-form.component';

describe('NouveauArticleFormComponent', () => {
  let component: NouveauArticleFormComponent;
  let fixture: ComponentFixture<NouveauArticleFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NouveauArticleFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NouveauArticleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
