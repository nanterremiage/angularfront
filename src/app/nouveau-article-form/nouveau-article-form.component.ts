import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AjoutArticle } from '../entite/ajoutArticle';
import { Article } from '../entite/article';
import { OrdinateursService } from '../services/ordinateurs.service';

@Component({
  selector: 'app-nouveau-article-form',
  templateUrl: './nouveau-article-form.component.html',
  styleUrls: ['./nouveau-article-form.component.scss']
})
export class NouveauArticleFormComponent implements OnInit {

  categories: any
  ajoutArticle: any
  result: any;

  constructor(private ordiService: OrdinateursService) { }

  ngOnInit(): void {
    this.ordiService.getCategories().subscribe(result => { 
      this.categories = result;
    });
  }

  ajout(ajoutForm:NgForm){
    this.ajoutArticle = <AjoutArticle>{ };
    this.ajoutArticle.article = <Article>{ };
    this.ajoutArticle.article.libelle=ajoutForm.value.libelle;
    this.ajoutArticle.article.marque=ajoutForm.value.marque;
    this.ajoutArticle.article.prix=ajoutForm.value.prix;
    this.ajoutArticle.article.photo=ajoutForm.value.photo;
    this.ajoutArticle.idCategorie=ajoutForm.value.categorie
    this.ordiService.ajoutArticle(this.ajoutArticle).subscribe(result => {
      this.result = result.ajout;
      ajoutForm.resetForm();
    });
  }
}
