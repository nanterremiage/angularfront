import { Article } from "./article";
import { Categorie } from "./categorie";

export interface ResponseListeArticles {
    categorie?: Categorie;
    articles?:Array<Article>;
}