export interface Categorie {
    id?: number;
    nom?:string;
    nomComplet?:string;
    sousCategorie?:Array<Categorie>;
}