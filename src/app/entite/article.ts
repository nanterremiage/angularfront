import { Categorie } from "./categorie";

export interface Article {
    id?: number;
    libelle?:string;
    marque?:string;
    categorie?:Categorie;
    prix?:number;
    photo?:string;
}