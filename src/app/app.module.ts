import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";
import { ListeArticlesComponent } from './liste-articles/liste-articles.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { NouveauArticleFormComponent } from './nouveau-article-form/nouveau-article-form.component';
import { ModifierArticleComponent } from './modifier-article/modifier-article.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    HelloWorldComponent,
    ConnexionComponent,
    ListeArticlesComponent,
    DetailArticleComponent,
    NouveauArticleFormComponent,
    ModifierArticleComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
