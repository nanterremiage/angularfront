import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Article } from '../entite/article';
import { OrdinateursService } from '../services/ordinateurs.service';

@Component({
  selector: 'app-liste-articles',
  templateUrl: './liste-articles.component.html',
  styleUrls: ['./liste-articles.component.scss']
})
export class ListeArticlesComponent implements OnInit, OnDestroy {

  list: any;
  name: any;
  nomComplet:any;
  navigationSubscription;


  constructor(private ordiService: OrdinateursService, private route: ActivatedRoute, private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.recupArticles();
      }
    });
  }
  
  recupArticles(){
    this.name = this.route.snapshot.paramMap.get('name');
    this.list= null;
    this.ordiService.getArticlesOfCategorie(this.name).subscribe(result => { 
      if(result!=null){
        this.nomComplet = result.categorie?.nomComplet;
      }
      else{
        this.nomComplet = "Pas d'éléments"
      }
      this.list = result.articles;
    });
  }


  ngOnInit(): void {
    this.recupArticles();
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

}
