import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.scss']
})
export class HelloWorldComponent implements OnInit {

  nom_Article:String="Chemise";
  taille_Article:number=40;
  couleur_Article:String="Blue";
  message:String="Achat : pas encore effectué";
  confirmation:boolean=false;

  constructor() { }

  ngOnInit(): void {
  }

  OnAchatArticle(){
    this.confirmation=true;
    this.message="Achat effectué avec succès";
  }

}
