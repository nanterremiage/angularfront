import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AuthentificationService } from '../services/authentification.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit,OnDestroy {

  navigationSubscription;
  logged:any;


  constructor(private authService:AuthentificationService,private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.isLogedIn();
      }
    });
   }

  ngOnInit(): void {
    this.isLogedIn();
  }

  isLogedIn(){
    this.logged = this.authService.isConnected();
  }

  disconnect(){
    this.authService.disconnect();
    this.router.navigate(['']);
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

}
