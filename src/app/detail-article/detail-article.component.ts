import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Article } from '../entite/article';
import { AuthentificationService } from '../services/authentification.service';
import { OrdinateursService } from '../services/ordinateurs.service';

@Component({
  selector: 'app-detail-article',
  templateUrl: './detail-article.component.html',
  styleUrls: ['./detail-article.component.scss']
})
export class DetailArticleComponent implements OnInit, OnDestroy {

  article: Article;
  id: any;
  navigationSubscription;
  logged: any;


  constructor(private ordiService: OrdinateursService, private route: ActivatedRoute, private router: Router, private location: Location, private authService: AuthentificationService) {
    this.article = <Article>{};
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.recupArticles();
        this.logged = authService.isConnected();
      }
    });
  }

  recupArticles() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.ordiService.getArticle(this.id).subscribe(result => {
      this.article = result;
    });
  }

  delete() {
    if (this.logged) {
      this.ordiService.deleteArticle(this.id).subscribe(result => {
      });
      this.router.navigate(['/Categorie/' + this.article.categorie?.nom]);
    }
    //this.cancel();
  }

  cancel() {
    this.location.back();
  }

  ngOnInit(): void {
    this.logged = this.authService.isConnected();
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

}
