import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../entite/article';
import { ResponseListeArticles } from '../entite/responseListeArticles';
import { Categorie } from '../entite/categorie';
import { AjoutArticle } from '../entite/ajoutArticle';
import { ResponseAjoutArticle } from '../entite/responseAjoutArticle';
import { Utilisateur } from '../entite/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class OrdinateursService {

  url = 'http://localhost:8080/TPWSJRSAPP/boutique/';

    constructor(private http: HttpClient) { }

  getArticlesOfCategorie(categorie:String): Observable<ResponseListeArticles>{
    return this.http.get<ResponseListeArticles>(this.url+'categorie/'+categorie+'/articles');
  }

  getArticle(id:number): Observable<Article>{
    return this.http.get<ResponseListeArticles>(this.url+'article/'+id);
  }

  getCategories(): Observable<Array<Categorie>>{
    return this.http.get<Array<Categorie>>(this.url+'categories');
  }

  ajoutArticle(ajoutArticle:AjoutArticle): Observable<ResponseAjoutArticle>{
    return this.http.post<ResponseAjoutArticle>(this.url+'articles',ajoutArticle);
  }

  modifArticle(article:Article): Observable<ResponseAjoutArticle>{
    return this.http.put<ResponseAjoutArticle>(this.url+'articles',article);
  }

  deleteArticle(id:number): Observable<ResponseAjoutArticle>{
    return this.http.delete<ResponseAjoutArticle>(this.url+'article/'+id);
  }

  connexion(utilisateur:Utilisateur): Observable<Utilisateur>{
    return this.http.post<Utilisateur>(this.url+'login',utilisateur);
  }

}
