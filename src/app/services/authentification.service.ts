import { Injectable } from '@angular/core';
import { Utilisateur } from '../entite/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  private utilisateur: Utilisateur;

  constructor() {
    this.utilisateur = <Utilisateur>{ };
    try {
      this.utilisateur = JSON.parse(localStorage.getItem('user') as string);
    } catch (error) {
      console.log(error);
    }
  }

  isConnected():boolean{
    if(this.utilisateur!=null){
      return this.utilisateur.email!=null;
    }
    return false;
  }

  disconnect(){
    this.utilisateur = <Utilisateur>{ };
    localStorage.removeItem('user');
  }

  connect(utilisateur:Utilisateur){
    localStorage.setItem('user', JSON.stringify(utilisateur));
    this.utilisateur=utilisateur;
  }

}
