import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';import { HelloWorldComponent } from './hello-world/hello-world.component';
import { HomeComponent } from './home/home.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { ListeArticlesComponent } from './liste-articles/liste-articles.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { NouveauArticleFormComponent } from './nouveau-article-form/nouveau-article-form.component';
import { ModifierArticleComponent } from './modifier-article/modifier-article.component';
import { NoauthentificationGuardGuard } from './guards/noauthentification-guard.guard';
import { AuthentificationGuardGuard } from './guards/authentification-guard.guard';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'Categorie/:name', component:ListeArticlesComponent },
  { path: 'Article/:id', component:DetailArticleComponent },
  { path: 'HelloWorld', component: HelloWorldComponent },
  { path: 'Connexion', component: ConnexionComponent,canActivate:[NoauthentificationGuardGuard] },
  { path: 'Ajout', component: NouveauArticleFormComponent, canActivate:[AuthentificationGuardGuard] },
  { path: 'Modif/:id', component: ModifierArticleComponent, canActivate:[AuthentificationGuardGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
