import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Article } from '../entite/article';
import { Categorie } from '../entite/categorie';
import { OrdinateursService } from '../services/ordinateurs.service';

@Component({
  selector: 'app-modifier-article',
  templateUrl: './modifier-article.component.html',
  styleUrls: ['./modifier-article.component.scss']
})
export class ModifierArticleComponent implements OnInit {

  categories: any
  modifArticle: Article
  result: any;
  id:any;
  idCategorie:any;

  constructor(private ordiService: OrdinateursService,private route: ActivatedRoute) {
    this.modifArticle = <Article>{ };
    this.modifArticle.categorie=<Categorie>{ };
  }

  ngOnInit(): void {
    this.ordiService.getCategories().subscribe(result => { 
      this.categories = result;
    });
    this.id = this.route.snapshot.paramMap.get('id');
    this.ordiService.getArticle(this.id).subscribe(result => { 
      this.modifArticle = result;
      this.idCategorie=this.modifArticle.categorie?.id;
      //console.log(this.idCategorie);
    });
    //console.log(this.modifArticle.libelle);
  }

  modif(modifForm:NgForm){
    if(modifForm.value.libelle){
      this.modifArticle.libelle=modifForm.value.libelle;
    }
    if(modifForm.value.marque){
      this.modifArticle.marque=modifForm.value.marque;
    }
    if(modifForm.value.prix){
      this.modifArticle.prix=modifForm.value.prix;
    }
    if(modifForm.value.photo){
      this.modifArticle.photo=modifForm.value.photo;
    }
    if(modifForm.value.categorie){
      this.modifArticle.categorie = <Categorie>{ };
      this.modifArticle.categorie.id = modifForm.value.categorie ;
      //console.log(this.modifArticle);
    }
    
    this.ordiService.modifArticle(this.modifArticle).subscribe(result => {
      this.result = result.ajout;
    });
  }

}
